/////////////////////
//// INIT MODULE ////
/////////////////////
var InitModule = (function(Modernizr) {
  'use strict';


  ////////////////////////////
  //// VARIABLES PRIVADAS ////
  ////////////////////////////
  var vars = {},
    methods = {},
    viewToLoad = location.search.substring(1),
    mobileDetection,
    browserDetection;




  ////////////////////////////
  //// VARIABLES GLOBALES ////
  ////////////////////////////
  vars = {
    isMobile: false,
    firefox: false,
    ie9: false,
    ie10: false,
    ie11: false
  };






  //////////////////////////
  //// METODOS PRIVADOS ////
  //////////////////////////

  function centertextslider() {
    $('#slides .slide_center').css({
      position: 'absolute',
      //  left: ($(window).width() - $('.slide_center').outerWidth())/2,
      top: (($(window).height() - $('.slide_center').outerHeight()) / 2) - 20
    });
  }

  mobileDetection = function() {
    if (window.Detectizr.device.type !== 'desktop' || Modernizr.mq('(max-width: 1200px)')) {
      vars.isMobile = true;
    } else {
      vars.isMobile = false;
    }
  };


  browserDetection = function() {
    if (window.Detectizr.browser.name === 'ie') {
      if (window.Detectizr.browser.major === '9') {
        vars.ie9 = true;
      }
      if (window.Detectizr.browser.major === '10') {
        vars.ie10 = true;
      }
      if (window.Detectizr.browser.major === '11') {
        vars.ie11 = true;
      }
      if (window.Detectizr.browser.major < 9) {
        $('.update-browser').show(0);
        $('.main').hide(0);
      }
    }

    if (vars.isMobile && Modernizr.mq('(max-width: 321px)')) {
      $('.slides-container li').css('width', '100%');
    }
    if (window.Detectizr.browser.name === 'chrome') {
      /*$(".mcol").css('padding-top', '0px', 'important'); */
      $(".submenu li a").css("cssText", "font-size: 15px !important;margin-top: 5px");

    }

  };






  //////////////////////////
  //// METODOS PUBLICOS ////
  //////////////////////////

  methods.ready = function() {

    $('.ir-arriba').click(function() {
      $('body, html').animate({
        scrollTop: '0px'
      }, 300);
    });

    $(window).scroll(function() {
      if ($(this).scrollTop() > 0) {
        $('.ir-arriba').slideDown(300);
      } else {
        $('.ir-arriba').slideUp(300);
      }
    });


    $('body').hide();
    $('body').fadeIn(800);
    centertextslider();

    //DETECTAR SI ES MOBILE
    mobileDetection();

    //DETECTAR NAVEGADORES
    browserDetection();


    //CARGA DE VISTAS (SOLO EN FRONTEND)
    if (viewToLoad.length > 0) {
      $('.main-content').load(viewToLoad + '.html', function() {
        //INICIAR
        methods.init();
      });
    } else {
      //INICIAR
      methods.init();
    }
  };


  methods.init = function() {

    $('.bxslider').bxSlider({
      mode: 'fade',
      adaptiveHeight: true,
      captions: true
    });


    $("#nav_top a").hover(function() {
      var cla = $(this).attr('class');
      $('#' + cla).removeClass('hidden');

      $("#slides").hover(function() {
        $('#' + cla).addClass('hidden');
      });

    });

    jQuery(function ($) {
       $('.panel-heading span.clickable').on("click", function (e) {
               if ($(this).hasClass('panel-collapsed')) {
               // expand the panel
               $(this).parents('.panel').find('.panel-body').slideDown();
               $(this).removeClass('panel-collapsed');
               $(this).find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
           }
           else {
               // collapse the panel
               $(this).parents('.panel').find('.panel-body').slideUp();
               $(this).addClass('panel-collapsed');
               $(this).find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
           }
       });
   });




    var $timeline_block = $('.cd-timeline-block');

    //hide timeline blocks which are outside the viewport
    $timeline_block.each(function() {
      if ($(this).offset().top > $(window).scrollTop() + $(window).height() * 0.75) {
        $(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
      }
    });

    //on scolling, show/animate timeline blocks when enter the viewport
    $(window).on('scroll', function() {
      $timeline_block.each(function() {
        if ($(this).offset().top <= $(window).scrollTop() + $(window).height() * 0.75 && $(this).find('.cd-timeline-img').hasClass('is-hidden')) {
          $(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
        }
      });
    });

    var $timeline_block1 = $('.naval-content-product');

    //hide timeline blocks which are outside the viewport
    $timeline_block1.each(function() {
      if ($(this).offset().top > $(window).scrollTop() + $(window).height() * 0.75) {
        $(this).find('.imagen-left, .navales-content').addClass('is-hidden');
      }
    });

    //on scolling, show/animate timeline blocks when enter the viewport
    $(window).on('scroll', function() {
      $timeline_block1.each(function() {
        if ($(this).offset().top <= $(window).scrollTop() + $(window).height() * 0.75 && $(this).find('.imagen-left').hasClass('is-hidden')) {
          $(this).find('.imagen-left, .navales-content').removeClass('is-hidden').addClass('bounce-in');
        }
      });
    });



    jQuery("document").ready(function($) {
      var nav = $('.menu-principal');
      var pos = nav.offset().top;


      $(window).scroll(function() {
        var fix = ($(this).scrollTop() > pos) ? true : false;
        nav.toggleClass("menu-principal-fixed", fix);
        //  $('body').toggleClass("fix-body", fix);

      });
    });



    //******************SLIDES************************************

    if ($('#slides').length > 0) {
      $('#slides').superslides({
        // hashchange: true,
        play: 5000,
        pagination: true
      });
    }



    //BOTONES
    methods.eventsList();

    //IMPEDIR EFECTO ROLLOVER EN MÓVILES
    if (vars.isMobile) {
      $('.hover').removeClass('hover');
    }
  };


  methods.eventsList = function() {
    //FUNCIONES QUE SE DEBEN EJECUTAR EN EL RESIZE
    $(window).on('resize', methods.resizeActions);
  };


  methods.resizeActions = function() {

    centertextslider();

    mobileDetection();

    browserDetection();
  };






  return {
    methods: methods,
    vars: vars
  };


})(Modernizr);





//CUANDO HA CARGADO EL DOM
$(document).ready(InitModule.methods.ready);
